Title: A New Home
Date: 2018-06-14 00:00:00


Getting back into the swing of things, I need a new home. I want to host my own server, but under current circumstances I’m unable to do so. For now, I’m going to be using a VPS. Setting one up was quick and affordable and will definitely work for now.

Keeping things simple. I set up [NGINX](https://en.wikipedia.org/wiki/Nginx) and registered a domain with a provider. After getting everything set up, I went for a sturdy operating system, [Debian](https://www.debian.org/). While I usually run [ArchLinux](https://www.archlinux.org/). They’re a bit too quick to add unstable software to their repositories.

To get my web server back up and running I wanted to go with another easy approach. Using a static site generator I’m able to post without having to deal with the nightmare of a CMS like WordPress. The software I chose was out of recommendation, [Jekyll](https://jekyllrb.com/). I like my post in [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). This gives them a good appearance without much coding.

For now, I’m using the default template. Which is fine for me, I will be able to spruce it up a bit more in the future. By that of course, I will be stripping a lot of things out actually. More on that later.

Moving forward, this setup will definitely work until I can get my home-lab up and running. I’m hopeful for the future and hope that I can continue to grow as I get back into programming. One step at a time!


