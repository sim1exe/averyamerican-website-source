Title: Project Euler: Part 1 - A Challenger Approaches
Date: 2019-10-18 9:30

I started working on Project Euler challenges to increase my knowledge of mathematics, algorithms, and my preferred coding language Python. The problems are designed to encourage computer programming to solve them. The goal is to move through each problem, in series starting at one until I reach the current challenge presented. This should give me a significant understanding of Python while also providing - hopefully fun, challenges along the way.

Problems one through nine were fairly easy and straightforward, with one problem in particular taking a significant portion of my time totaling at about 7 hours to complete. The problem was to find the largest prime factor in 600,851,475,143. The issue I was having finding the solution was in how I was performing the primality test. I would go through every iteration of the numbers between 2 and 300 billion. This quickly became a time consuming process. The solution towards speeding up this process required a bit of research into algorithms. It becomes an order of magnitude more efficient to only go towards the square root of a number because once you hit the square root, you're moving beyond the halfway point.

<center>![Alt Text]({static}/images/half-sqrt-compare.png)</center>

It was around this time that I noticed my mindset for solving programming began to shift and change slightly. Something that doesn't really happen in the tutorials that you can find on the internet. You began to look at your code slightly differently, as you're already searching for ways to get your language to do more, with less. This doesn't mean less code is faster, it is about efficiency. Computers are fast, however you can only perform a set number of operations per minute. And it’s your job to use as few operations as possible to get the solution you're after. After some growth in how I viewed these challenges, they became progressively easier for a while. I am glad I decided to work through these challenges and will continue to do so until I reach the current problems.

After some growth in how I viewed these challenges, they became progressively easier for a while. I am glad I decided to work through these challenges and will continue to do so until I reach the current problems.

